require 'test_helper'

class SchoolTest < ActiveSupport::TestCase
def setup
    @school = School.new(schoolName: "Pella", displayName: "Pella" , classification: "3A", cooperativeSchool:"", active:"yes")
end

  test "should be valid" do
    assert @school.valid?
  end

  test "name should be present" do
    @school.schoolName = "     "
    assert_not @school.valid?
  end
end
