require 'test_helper'

class RankerTest < ActiveSupport::TestCase
  def setup
    @ranker = Ranker.new(firstName:"dnjf", lastName:"sfda", email:"dkfn", rankAccess:"dsjk")
  end
  
  test "should be valid" do
    assert @ranker.valid?
  end
  
  test "email should be present" do
    @ranker.email = "   "
    assert_not @ranker.valid?
  end
  
  test "email should not be more than 255 characters" do
    @ranker.email = "a"*244 + "@example.com"
    assert_not @ranker.valid?
  end
  
end
