require 'test_helper'

class CreateRankersTest < ActionDispatch::IntegrationTest
  test "invalid signup information" do
    get rankers_path
    assert_no_difference 'Ranker.count' do
      post create_path, ranker: { firstName:  "",
                               lastName:"",
                               email: "user@invalid",
                               password_digest:"foo",
                               rankAccess:""
                                }
    end
    assert_template 'rankers/new'
  end

end
