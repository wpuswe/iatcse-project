class RankingsController < ApplicationController
  def new
     @ranking = Ranking.new
  end

  def edit
     @ranking = Ranking.find(params[:id])
  end

  def update
      @ranking = Ranking.find(params[:id])
     if @ranking.update_attributes(ranking_params)
      redirect_to @ranking
     else
      render 'edit'
     end
  end

  def index
      @rankings = Ranking.all
  end

  def show
       @ranking = Ranking.find(params[:id])
  end

  def create
     @ranking = Ranking.new(ranking_params)    
    if @ranking.save
      flash[:success] = "Ranking was added successfully!"
      redirect_to @ranking   
    else
      render 'new'
    end
  end
  
  def delete
  end

  def destroy
   Ranking.find(params[:id]).destroy
   flash[:success ]="Ranking Week deleted successively!"
   redirect_to rankings_url
  end

  def ranking_params
    params.require(:ranking).permit(:rankingTitle, :gender, :classification, :deadline, :publishDate, :voteTotal)
  end
end
