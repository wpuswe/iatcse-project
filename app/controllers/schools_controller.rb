class SchoolsController < ApplicationController
  def new
      @school = School.new
  end

  def create
     @school = School.new(school_params)    
    if @school.save
      flash[:success] = "School was added successfully!"
      redirect_to @school   
    else
      render 'new'
    end
  end

  def edit
    @school = School.find(params[:id])
  end
  
  def show
    @school = School.find(params[:id])
  end

  def update
     @school = School.find(params[:id])
     if @school.update_attributes(school_params)
      redirect_to @school
     else
      render 'edit'
     end
  end
  

   
 def index
  if params[:classification]
    @school = School.where(:classification => params[:classification])
  else
    @school = School.all
  end
 end
    
  
  
  def delete
  end

  def destroy 
   School.find(params[:id]).destroy
   flash[:success ]="School deleted successively!"
    redirect_to schools_url
  end

  
   private

    def school_params
      params.require(:school).permit(:schoolName, :displayName, :classification, :active,:cooperativeSchool)
    end
  

end
