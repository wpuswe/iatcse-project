class RankersController < ApplicationController
  
  wrap_parameters :ranker, include: [:firstName, :lastName, :email, :password, :password_confirmation, :rankAccess, :active]
  def new
    @ranker = Ranker.new
  end
  
  def edit
    @ranker = Ranker.find(params[:id])
  end
  
  def update
    @ranker = Ranker.find(params[:id])
    if @ranker.update_attributes(ranker_params)
      redirect_to @ranker
    else
      render 'edit'
    end
  end

  def index
    @rankers = Ranker.all
  end
  
  def show
    @ranker = Ranker.find(params[:id])
  end
  
  def destroy 
   Ranker.find(params[:id]).destroy
   flash[:success ]="Ranker deleted successively!"
    redirect_to rankers_url
  end
  
  def create
    @ranker = Ranker.new(ranker_params)    
    if @ranker.save
      flash[:success] = "Welcome to the Ranking App!"
      redirect_to @ranker   
    else
      render 'new'
    end
  end
    private

    def ranker_params
      params.require(:ranker).permit(:firstName, :lastName, :email, :password, :password_confirmation, :rankAccess, :active)
    end

end
