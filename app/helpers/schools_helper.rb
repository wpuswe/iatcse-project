module SchoolsHelper
  
  def classForDisplay(classification)
    if (params[:classification] == classification && params[:classification].present?) 
      return "btn btn-default disabled"
    else
      return "btn btn-default"
    end
  end
end
