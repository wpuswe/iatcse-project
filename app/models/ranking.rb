class Ranking < ActiveRecord::Base
  has_many :votes
  has_many :rankers, through: :votes
  validates :rankingTitle, presence: true, length: { maximum: 50 }
  validates :gender, presence: true, length: { maximum: 50 }
  validates :classification, presence: true, length: { maximum: 10 }
  validates :deadline, presence: true
  validates :publishDate, presence: true
  validates :voteTotal, presence: true, length: { maximum: 50 }
  validate :validateDateTime
  
  private
  def validateDateTime
    if deadline != nil && publishDate != nil
    if deadline > publishDate
      errors.add(:deadline, "must be before or same as publish date")
    end
    end
  end
end
