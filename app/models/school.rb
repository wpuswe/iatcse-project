class School < ActiveRecord::Base
 scope :'2A', -> {
  where(:classification => "2A")
}
 scope :'4A', -> {
  where(:classification => "4A")
}
  scope :'3A', -> {
  where(:classification => "3A")
}
scope :'1A', -> {
  where(:classification => "1A")
}
  
  
  validates :schoolName, presence: true, length: { maximum: 50 }
  validates :classification, presence: true, length: { maximum: 50 }
  validates :displayName, presence: true, length: { maximum: 50 }
  validates :cooperativeSchool, length: { maximum: 50 }
  validates :active, presence: true, length: { maximum: 5 }
 
 
end
 