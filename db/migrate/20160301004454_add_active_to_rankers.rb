class AddActiveToRankers < ActiveRecord::Migration
  def change
    add_column :rankers, :active, :integer
  end
end
