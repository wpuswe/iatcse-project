class CreateRankings < ActiveRecord::Migration
  def change
    create_table :rankings do |t|
      t.string :rankingTitle
      t.string :gender
      t.string :classification
      t.date :deadline
      t.date :publishDate
      t.string :voteTotal

      t.timestamps null: false
    end
  end
end
