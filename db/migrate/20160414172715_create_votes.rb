class CreateVotes < ActiveRecord::Migration
  def change
    create_table :votes do |t|
      t.string :votes
      t.string :completed

      t.timestamps null: false
    end
  end
end
