class CreateSchools < ActiveRecord::Migration
  def change
    create_table :schools do |t|
      t.string :schoolName
      t.string :displayName
      t.string :classification
      t.string :cooperativeSchool
      t.string :active

      t.timestamps null: false
    end
  end
end
