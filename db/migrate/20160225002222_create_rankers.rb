class CreateRankers < ActiveRecord::Migration
  def change
    create_table :rankers do |t|
      t.string :firstName
      t.string :lastName
      t.string :email
      t.string :rankAccess
      
      t.timestamps null: false
    end
  end
end
