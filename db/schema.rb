# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160414172715) do

  create_table "rankers", force: :cascade do |t|
    t.string   "firstName"
    t.string   "lastName"
    t.string   "email"
    t.string   "rankAccess"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
    t.string   "password_digest"
    t.integer  "active"
  end

  create_table "rankings", force: :cascade do |t|
    t.string   "rankingTitle"
    t.string   "gender"
    t.string   "classification"
    t.date     "deadline"
    t.date     "publishDate"
    t.string   "voteTotal"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
  end

  create_table "schools", force: :cascade do |t|
    t.string   "schoolName"
    t.string   "displayName"
    t.string   "classification"
    t.string   "cooperativeSchool"
    t.string   "active"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
  end

  create_table "votes", force: :cascade do |t|
    t.string   "votes"
    t.string   "completed"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

end
